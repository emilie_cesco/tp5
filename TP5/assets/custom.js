
let Btnrechercher = document.getElementById('btnRechercher')
let sliceperso = document.getElementById('sliceperso')
let maRecherche = document.getElementById('inputText')
maRecherche.value = ""
let result = document.getElementById('result');
let urlPoster = `https://image.tmdb.org/t/p/w185`;
let urlPosterBig = `https://image.tmdb.org/t/p/original`;

let form1 = document.getElementById('form1');

let pagination = document.getElementById('divPagination')

//déclaration variables pagination
let linkimg
let btnprecedent = document.getElementById('btnprecedent')
let btnsuivant = document.getElementById('btnsuivant')
let pag1 = document.getElementById('pag1')
let pag2 = document.getElementById('pag2')
let pag3 = document.getElementById('pag3')
let pag4 = document.getElementById('pag4')


//fetch film populaire + affiche des films sur html
let filmpop = document.getElementById('film-populaire')

//event pagination 
pag1.addEventListener('click', e => { refresh(), fetchfilms(maRecherche.value, 0, 5) })
pag2.addEventListener('click', e => { refresh(), fetchfilms(maRecherche.value, 5, 10) })
pag3.addEventListener('click', e => { refresh(), fetchfilms(maRecherche.value, 10, 15) })
pag4.addEventListener('click', e => { refresh(), fetchfilms(maRecherche.value, 15, 20) })


Btnrechercher.addEventListener('click', e => {
  refresh()
  fetchfilms(maRecherche.value, 0, 10)

})

maRecherche.addEventListener('keyup', e => {

  fetchfilms(maRecherche.value, 0, 5)
  if (maRecherche.value.length <= 0) {
    result.style.display = "none";
    filmpop.style.display = 'flex'

  }
  else {
    result.style.display = "block";
    filmpop.style.display = 'none';
    
  }
  refresh()

})

maRecherche.addEventListener('mouseover', e => {
  if(maRecherche.value.length > 0 ){
    result.style.display = "block";
  }
})

// maRecherche.addEventListener('mouseleave', e => {
//     result.style.display = "none";
// })

result.addEventListener('mouseleave', e => {
result.style.display = "none";

})


document.addEventListener("click", e => {
  if (e.target.getAttribute("data-movieid")) {
    localStorage.setItem('movieid', e.target.getAttribute("data-movieid"));
    window.location = "pagefilms.html"
  }
})




//class div result
result.classList.add('shadow')

function fetchfilms(search, a, b) {
  form1.innerHTML = ''
  fetch(`https://api.themoviedb.org/3/search/movie?api_key=1162948aeb909831f20f86bd8ca90466&language=fr-FR&query=${search}&page=1&include_adult=true`)
    .then((response) => response.json())

    .then((json) => {



      if (json.total_results == 0) {
        let noresult = document.createElement('p')
        noresult.innerHTML = 'Aucun résultat trouvé';
        result.append(noresult)
        return
      }

      //boucle sur le nombre d'éléments du tableau

      let slicejson = json.results.slice(a, b);
      // console.log(slicejson)
      for (i = 0; i < slicejson.length; i++) {
        // console.log(slicejson[i])


        //menu déroulant input

        let base = document.createElement('div')
        base.classList.add('div-input');
        base.dataset.movieid = slicejson[i].id;

        let title = document.createElement('a')
        title.innerHTML = slicejson[i].title
        title.setAttribute('class',' div-p')
        // title.setAttribute('href', `slicejson[i].id`)
        title.dataset.movieid = slicejson[i].id;
        

        let img = document.createElement('img')
        img.setAttribute('src', `https://image.tmdb.org/t/p/w500/${slicejson[i].poster_path}`)
        img.style.width = '50px'
        img.classList.add('img-input')
        title.dataset.movieid = slicejson[i].id;


        //mes cards 

        form1.classList.add('container-fluid', 'row');

        let basediv = document.createElement('div');
        basediv.classList.add('baseDiv', 'card', 'mx-3', 'my-3', 'mb-4', 'shadow', 'h-100');
        basediv.style.marginBottom = "20px"



        let titlediv = document.createElement('p');
        titlediv.innerHTML = slicejson[i].title
        titlediv.classList.add('titleDiv', 'text-center');
        titlediv.style.fontWeight = "bolder";
        titlediv.style.color = " 	#404040";

        let datediv = document.createElement('p');
        datediv.innerHTML = slicejson[i].release_date.split("-")[0]
        datediv.classList.add('genreDiv', 'text-center');
        datediv.style.color = " 	#404040";

        let lienimg = document.createElement('a')
        lienimg.classList.add('m-1')


        let imgdiv = document.createElement('img')
        imgdiv.setAttribute('src', `https://image.tmdb.org/t/p/w500/${slicejson[i].poster_path}`)
        imgdiv.style.width = '220px'
        imgdiv.classList.add('imgDiv', 'align-item-center')
        imgdiv.dataset.movieid = slicejson[i].id;



        base.append(img, title)
        form1.appendChild(lienimg)
        lienimg.appendChild(basediv)
        basediv.appendChild(imgdiv)
        basediv.appendChild(titlediv)
        basediv.appendChild(datediv)

        result.append(base);

      }

      // console.log(json.results)

    })
}

function fetchfilmpopulaire() {

  fetch(`https://api.themoviedb.org/3/movie/popular?api_key=1162948aeb909831f20f86bd8ca90466&language=fr-FR&page=1`)
    .then((response) => response.json())

    .then((json) => {

      for (i = 0; i < json.results.length; i++) {
        // console.log(json.results[i])

       

        let img = document.createElement('img');
        img.setAttribute('src', `https://image.tmdb.org/t/p/w200/${json.results[i].poster_path}`);
        img.classList.add('img-pop','d-flex','card','shadow-lg','mr-2')
        img.dataset.movieid = json.results[i].id;
        

        filmpop.append(img);


      }


    })

};

fetchfilmpopulaire();

//boucle inversée pour le refresh
const refresh = () => {

  for (let i = result.children.length - 1; i >= 0; --i) {
    result.removeChild(result.children[i])
  }
}




