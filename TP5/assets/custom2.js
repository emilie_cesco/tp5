let pagefilmimage = document.getElementById('pagefilmimage')
let data = localStorage.getItem('movieid');

console.log(data)

function fetchdata() {
  fetch(`https://api.themoviedb.org/3/movie/${data}?api_key=1162948aeb909831f20f86bd8ca90466&language=en-US`)
    .then((response) => response.json())

    .then((json) => {


      let divBg = document.getElementById('img-bg-films')
      divBg.style.backgroundImage = `url(https://image.tmdb.org/t/p/original/${json.backdrop_path})`
      divBg.classList.add('shadow')


      console.log(json)


      let poster = document.getElementById('poster')
      poster.classList.add('img-poster', 'img-fluid', 'shadow-lg')
      poster.setAttribute('src', `https://image.tmdb.org/t/p/original/${json.poster_path}`)
      poster.style.width = '380px'
      poster.style.height = '550px'
      poster.style.border = 'solid rgba(0, 0, 0, 0.521)'

      
      let divTitre = document.getElementById('divTitre')
      divTitre.innerHTML = '- ' + json.title

      let divGenre = document.getElementById('divGenre')
      divGenre.innerText = `${json.genres[0].name} , ${json.genres[1].name}`
     


      let divDate = document.getElementById('divDate')
      divDate.classList.add('float-left','mr-3' )
      divDate.innerText = json.release_date.split("-")[0]
      divTitre.append(divDate)

      let divSyno = document.getElementById('divSyno')
      divSyno.innerText = json.overview





    })
};

fetchdata();


